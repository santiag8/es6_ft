import {DemoCtrl} from './main';
import angular from 'angular';

angular.module('app', [])
  .controller('DemoCtrl', DemoCtrl);
